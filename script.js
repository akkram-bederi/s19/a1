console.log(`Activity 19`);

let getCube= 2**3;
console.log(`The cube of 2 is ${getCube}`);

let address = [1810, `Antonio Rivera Street`, `Tondo`,`Manila`]

const [house,street,city, province] = address

console.log(`I live at ${house}, ${street}, ${city}, ${province}`);


let animal={
	species:`elephant`,
	name:`georgie`,
	weight:`4000kg`,
	where:`savannah`
}

const{species,name,weight,where}=animal;

console.log(`${name} is an ${species} that weighs ${weight} that was found in the ${where} `);


let nums=[1,2,3,4,5]

nums.forEach(nums => console.log(nums))

let reduceNumber = nums.reduce((num1,num2)=>num1+num2);
console.log(reduceNumber);


class Dog {
		constructor(name, age, type){
			this.name = name;
			this.age = age;
			this.type = type;
		}
	}

	let dog = new Dog(`Pikachu`,1,`electric`);
	console.log(dog);